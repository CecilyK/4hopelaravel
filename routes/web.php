<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/about', function () {
    return view('About');
});
Route::get('/adults', function () {
    return view('adults');
});

Route::get('/articles', function () {
    return view('articles');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/cause', function () {
    return view('Cause');
});
Route::get('/children_teens', function () {
    return view('children_teens');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/donate', function () {
    return view('donate');
});

Route::get('/events', function () {
    return view('events');
});

Route::get('/gallery', function () {
    return view('gallery');
});
Route::get('/on-site', function () {
    return view('on-site');
});
Route::get('/professionals', function () {
    return view('professionals');
});
Route::get('/single-blog', function () {
    return view('single-blog');
});
Route::get('/testimonials', function () {
    return view('testimonials');
});
Route::get('/volunteer', function () {
    return view('volunteer');
});
Route::get('/contact/index', function () {
    return view('contact/index');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('events','EventsContoller');