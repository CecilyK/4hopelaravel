<!doctype html>
<html class="no-js" lang="zxx">

<head>
@include('layouts.head')
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

  
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Children & Teens</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <!--================Blog Area =================-->
    <section>
        <div class="container">
            <h2>Campers</h2>
            <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
      
        <div class="row gap100">
            <div class="col-md-6" >
                   
                  
                    <p>The weekend-long camp is meant to provide the children an opportunity to learn coping skills,enjoy
                        camp activities and connect with other kids who are also grieving the loss or death of a close loved
                        one. The child should be between 7-17 and Death of loved one should have occurred at least 3
                        months before and within the past 4 years of the camp session date they wish to attend.</p>
                  <br><br>
                  <a href="{{ url('contact/index') }}">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>         <br>
           
                
                </div>
            <div class="col-md-6">
               <img src="{{ secure_asset('img/banner/camp.jpg') }}" class="img-fluid" alt="">
            </div>
          
           
        </div>
        <!-- <hr class="mt-1 mb-5" style="  border-top: 2px solid #bbb;"> -->
        </div>
    
    </section>


    <section>
        <div class="container">
            <h2>School-based program</h2>
            <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
      
        <div class="row gap100">
            <div class="col-md-6" >
                   
                  
                    <p>Many children and teens grieve alone and tend to experience masked grief. Each student will be
                        affected differently depending on his her developmental level,cultural beliefs,personal
                        characteristics,family situation and previous experiences. We have created a system that brings our services to local schools. The children can participate in our on-site grief support group in their own schools. The groups will
                        provide a safe place for children to share their thoughts and feelings with other children who are also
                        grieving the death of a family member or close relative. Students will be identified by school
                        personnel and will be invited to participate. Emotional stability enhances cognitive ability where it might have been compromised by heavy
                        emotions and improves social and behavioral performance.</p>
                  <br><br>
                <a href="{{ url('contact/index') }}">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>
                  <br>
           
                
                </div>
            <div class="col-md-6">
               <img src="{{ secure_asset('img/blog/school.jpg') }}" class="img-fluid" alt="">
            </div>
          
           
        </div>
        <hr class="mt-1 mb-5" style="  border-top: 2px solid #bbb;">
        </div>
    
    </section>

    <!--================ Blog Area end =================-->


    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->


    <!-- JS here -->
    <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
    <script>
        $('.datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-calendar"></span>'
            }
        });

        $('.timepicker').timepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-clock-o"></span>'
            }
        });
    </script>
</body>

</html>