<!doctype html>
<html class="no-js" lang="zxx">

<head>
@include('layouts.head')

    <style>
        a.custom-card,
a.custom-card:hover {
  color: inherit;
}
    </style>
</head>

<body>
   <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

     <!-- header-start -->
     <header>
     @include('layouts.header')
    </header>
    <!-- header-end -->

  <!-- bradcam_area_start  -->
  <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
      <div class="container">
          <div class="row">
              <div class="col-xl-12">
                  <div class="bradcam_text text-center">
                      <h3>On Site Grief Support</h3>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bradcam_area_end  -->

  <section>
    <div class="container">
        <h2>On-site grief support</h2>
        <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
  
    <div class="row gap100">
        <div class="col-md-6" >
               
              
                <p>Death,natural or unnatural disasters and unfortunate disasters in the workplace or school setting can
                    profoundly affect an organizations staff and community causing disruption that if left untreated can trigger
                    inhibited recovery of the employees and organization as a whole. This may lead to reduced concentration,a
                    drop in performance and attendance. 4Hope Together is readily available to provide on site grief support to help employees cope with the
                    aforesaid circumstances.</p>
              <br><br>
              <a href="contact/index.html">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>         <br>
   <br>
       
            
            </div>
        <div class="col-md-6">
           <img src="{{ secure_asset('img/banner/support.jpg') }}" class="img-fluid" alt="">
        </div>
      
       
    </div>

    <div class="row gap100">
        <div class="col-md-6">
            <h3>Service we Offer include</h3>
            <ul class="list-group list-group-flush" style="color: #444444;">
                <li class="list-group-item">On-site grief support in the early days following death</li>
                <li class="list-group-item">Specific grief education for the workplace</li>
                <li class="list-group-item">Follow up grief support if needed</li>
                 </ul>
    </div>

</div>
    <hr class="mt-1 mb-5" style="  border-top: 2px solid #bbb;">
    </div>

</section>

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

        <!-- link that opens popup -->

        <!-- JS here -->
        <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
  <script>
      $('.datepicker').datepicker({
          iconsLibrary: 'fontawesome',
          icons: {
              rightIcon: '<span class="fa fa-calendar"></span>'
          }
      });

      $('.timepicker').timepicker({
          iconsLibrary: 'fontawesome',
          icons: {
              rightIcon: '<span class="fa fa-clock-o"></span>'
          }
      });
  </script>
</body>

</html>