<div class="header-area ">
            <div class="header-top_area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 col-lg-8">
                            <div class="short_contact_list">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-phone"></i> 0700000000</a></li>
                                    <li><a href="#"> <i class="fa fa-envelope"></i>Yourmail@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-6 col-lg-4">
                            <div class="social_media_links d-none d-lg-block">
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-pinterest-p"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-3">
                            <div class="logo">
                                <a href="{{ url('/') }}">
                                    <!-- <img src="img/logo.png" alt=""> -->
                                    <p style="color: whitesmoke; font-weight: 800; font-size: larger;">4HopeOrg</p>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <div class="main-menu">
                                <nav>
                                    <!-- <div class="Appointment">
                                    <div class="book_btn d-none d-lg-block">
                                        <a data-scroll-nav='1' href="{{ url('donate') }}">Donate</a>
                                    </div>
                                </div> -->
                                    <ul id="navigation">
                                        <li><a href="#">Grief Support <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">

                                                <li><a href="{{ url('children_teens') }}">Children & Teens</a></li>
                                                <li><a href="{{ url('adults') }}">Adults</a></li>
                                                <li><a href="{{ url('professionals') }}">Professionals</a></li>
                                                <li><a href="{{ url('on-site') }}">On-site support</a></li>

                                            </ul>
                                        </li>
                                        <li><a href="{{ url('about') }}">About</a></li>
                                        <li><a href="#">Resources <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="{{ url('gallery') }}">Gallery</a></li>
                                                <li><a href="{{ url('testimonials') }}">Testimonials</a></li>
                                                <li><a href="{{ url('articles') }}">Articles</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="{{ url('events') }}">Events </i></a>
                                            <!-- <ul class="submenu">
                                                <li><a href="elements.html">elements</a></li>
                                                <li><a href="Cause.html">Cause</a></li>
                                            </ul> -->
                                        </li>
                                        <li><a href="{{ url('volunteer') }}">Volunteer</a></li>
                                        <!--<li><a href="{{ url('home') }}">Login as Admin</a></li>-->
                                        <li><a href="{{ url('donate') }}">Donate</a></li>
                                    </ul>
                                </nav>

                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>