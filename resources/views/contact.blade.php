<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.head')
  <link rel="stylesheet" href="{{ secure_asset('contact.css')}}">

 <script src="">
     //Inspired by a dribbble by Bluroon
//https://dribbble.com/shots/1356864-Contact-Us

//Capture the click event on a location
$("#location-bar a").click(function(event){
  event.preventDefault();
  
  var $this = $(this),
      $li = $this.parent(),
      selectedMap = $this.attr("href"),
      selectedLocation = $this.data('location');
 
  $li.addClass('active').siblings('li').removeClass('active');

  //Update #map bkimage with the image from the location
  $('#map').css('background-image', 'url(' + selectedMap + ')');  
  //update tooltip 'address'
  $('.selectedLocation').text(selectedLocation);
});
 </script>
</head>
<body>
 
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Articles</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->
    <div id='browser'>
        <div id='browser-bar'>
          <div class='circles'></div>
          <div class='circles'></div>
          <div class='circles'></div>
          <p>Contact Us</p>
          <span class='arrow entypo-resize-full'></span>
        </div>
        <div id='content'>
          <div id='left'>
            <div id='map'>
              <p>Where To Find Us</p>
              <div class='map-locator'>
                <div class='tooltip'>
                  <ul>
                    <li>
                      <span class='entypo-location'></span>
                      <span class='selectedLocation'>Israel</span>
                    </li>
                    <li>
                      <span class='entypo-mail'></span>
                      <a href='#'>johndoe@gmail.com</a>
                    </li>
                    <li>
                      <span class='entypo-phone'></span>
                      (+972 2) 629 06 32
                    </li>
                  </ul>
                </div>
              </div>
              <div class='zoom'></div>
            </div>
            <ul id='location-bar'>
              <li>
                <a>Israel</a>
              </li>
              <li>
                <a>USA</a>
              </li>
              <li>
                <a>The Netherlands</a>
              </li>
              <li>
                <a>Singapore</a>
              </li>
            </ul>
          </div>
          <div id='right'>
            <p>Connect</p>
            <div id='social'>
              <a class='social'>
                <span class='entypo-facebook'></span>
              </a>
              <a class='social'>
                <span class='entypo-twitter'></span>
              </a>
              <a class='social'>
                <span class='entypo-linkedin'></span>
              </a>
              <a class='social'>
                <span class='entypo-gplus'></span>
              </a>
              <a class='social'>
                <span class='entypo-instagrem'></span>
              </a>
            </div>
            <form>
              <p>Get in Contact</p>
              <input placeholder='Email' type='email'>
              <input placeholder='Subject' type='text'>
              <textarea placeholder='Message' rows='4'></textarea>
              <input placeholder='Send' type='submit'>
            </form>
            <p>other way</p>
            <p class='other entypo-mail'>
              <a href='#'>johndoe@gmail.com</a>
            </p>
            <p class='other entypo-phone'>(+972 2) 629 06 32</p>
          </div>
        </div>
      </div>
      
 
    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->


  <!-- link that opens popup -->

  <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
      




</body>
</html>
