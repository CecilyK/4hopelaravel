<!doctype html>
<html class="no-js" lang="zxx">

<head>
@include('layouts.head')

    <style>
        a.custom-card,
a.custom-card:hover {
  color: inherit;
}
    </style>
</head>

<body>
   <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->
  <!-- bradcam_area_start  -->
  <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
      <div class="container">
          <div class="row">
              <div class="col-xl-12">
                  <div class="bradcam_text text-center">
                      <h3>Adults</h3>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- bradcam_area_end  -->

  <section>
    <div class="container">
        <h2>Support Groups</h2>
        <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
  
    <div class="row gap100">
        <div class="col-md-6" >
               
              
                <p>In consideration of unavailability of some individuals to attend support group meetings physically due to
                    distance,geographical location and other unavoidable circumstances, 4Hope Together conducts virtual
                    meetings where members can still come together and talk about their experiences,thoughts and emotions
                    on loss and grief and still offer each other support.</p>
              <br><br>
              <a href="{{ url('contact/index') }}">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>     <br>
       
            
            </div>
        <div class="col-md-6">
           <img src="{{ secure_asset('img/banner/support.jpg') }}" class="img-fluid" alt="">
        </div>
      
       
    </div>

    <!-- <hr class="mt-1 mb-5" style="  border-top: 2px solid #bbb;"> -->
    </div>




</section>

<section>
    <div class="container">
        <h2>Virtual meetings</h2>
        <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
  
    <div class="row gap100">
        <div class="col-md-6" >
               
              
                <p>In consideration of unavailability of some individuals to attend support group meetings physically due to
                    distance,geographical location and other unavoidable circumstances, 4Hope Together conducts virtual
                    meetings where members can still come together and talk about their experiences,thoughts and emotions
                    on loss and grief and still offer each other support.</p>
              <br><br>
              <a href="{{ url('contact/index') }}">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>    <br>
         
              <div class="card text-center">
                <div class="card-header">
                  Online Meetings
                </div>
                <div class="card-body">
                  <!-- <h5 class="card-title"></h5> -->
                  <p class="card-text">Join us in our next online meeting. Click below to access the zoom meeting link</p>
                  <a href="#" class="btn btn-primary">Click here</a>
                </div>
                <div class="card-footer text-muted">
                  17/02/2000 20:00 E.A.T
                </div>
              </div>
          
          
            </div>
        <div class="col-md-6">
           <img src="{{ secure_asset('img/banner/virtual.jpg') }}" class="img-fluid" alt="">
        </div>
      
       
    </div>

    <hr class="mt-1 mb-5" style="  border-top: 2px solid #bbb;">
    </div>




</section>

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->


   <!-- JS here -->
   <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
  <script>
      $('.datepicker').datepicker({
          iconsLibrary: 'fontawesome',
          icons: {
              rightIcon: '<span class="fa fa-calendar"></span>'
          }
      });

      $('.timepicker').timepicker({
          iconsLibrary: 'fontawesome',
          icons: {
              rightIcon: '<span class="fa fa-clock-o"></span>'
          }
      });
  </script>
</body>

</html>