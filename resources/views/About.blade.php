<!doctype html>
<html class="no-js" lang="zxx">
    <head>
    @include('layouts.head')

     
        <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    </head>
<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>About us</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <section class="section-3">


        <div class="container text-center">
            <div class="row">
                <div class="col-md-6">
                    <div class="pray">
                        <img src="{{ secure_asset('img/banner/about.jpg') }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel text-left">
                        <h1>About Us</h1>
                        <p class="pt-4">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </p>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                            Earum ducimus, autem eaque maiores laudantium pariatur.</p>
                    </div>
                </div>
            </div>
        </div>

    </section>


    <section id="team" class="pb-5">
        <div class="container">
            <h5 class="section-title h1">OUR TEAM</h5>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p>
                                            <img class=" img-fluid" alt="Team Cards Flipper" src="http://www.prepbootstrap.com/Content/images/template/people/person1.jpg" /></p>
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <a href="#" class="btn btn-primary btn-sm">More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-skype"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-google"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p>
                                            <img class=" img-fluid" alt="Team Cards Flipper" src="http://www.prepbootstrap.com/Content/images/template/people/person2.jpg" /></p>
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <a href="#" class="btn btn-primary btn-sm">More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-skype"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-google"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p>
                                            <img class=" img-fluid" alt="Team Cards Flipper" src="http://www.prepbootstrap.com/Content/images/template/people/person3.jpg" /></p>
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <a href="#" class="btn btn-primary btn-sm">More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Web Developer</h4>
                                        <p class="card-text">The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-skype"></i>
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a class="social-icon text-xs-center" target="_blank" href="#">
                                                    <i class="fa fa-google"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
        </div>
    </section>
    

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

    <!-- link that opens popup -->

    <!-- JS here -->
    <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
</body>

</html>