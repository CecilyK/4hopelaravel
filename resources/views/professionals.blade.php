<!doctype html>
<html class="no-js" lang="zxx">

<head>
@include('layouts.head')
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
 
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->
    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Professionals</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <!--================Blog Area =================-->


    <section>
        <div class="container">
            <h2>Teachers</h2>
            <hr class="mt-3" style="  border-top: 1px solid #bbb; margin-bottom:-40px;">
      
        <div class="row gap100">
            <div class="col-md-6" >
                   
                  
                    <p>Professional grief support in schools is for teachers,counselors and administrators. It is obvious that
                        students spend more of their waking hours in schools.it is important for teachers to
                        Know and understand how to support grieving children,so that they can be able to adapt to the loss of
                        Their loved one hence curbing the physical,behavioral,cognitive and emotional problems brought
                        About by grief. 4HopeTogether provides training for teachers,administrators and counselors in schools.</p>
                  <br><br>
                  <a href="{{ url('contact/index') }}">  <button type="button" class="btn  btn-lg btn-block" style="background:#3cc78f; color:white;">Contact us for more details</button></a>         <br>
                  <br>
           
                
                </div>
            <div class="col-md-6">
               <img src="{{ secure_asset('img/blog/school.jpg') }}" class="img-fluid" alt="">
            </div>
          
           
        </div>
        <hr class="" style="  border-top: 2px solid #bbb;">
       

        <div class="row gap100">
        <div class="col-md-6">
            <h3>Topic examples include?</h3>
            <ul class="list-group list-group-flush" style="color: #444444;">
                <li class="list-group-item">What is grief</li>
                <li class="list-group-item">How to talk to children about funerals</li>
                <li class="list-group-item">How children and adolescents grieve</li>
                <li class="list-group-item">Supporting grieving families and children</li>
                <li class="list-group-item">Coping with death in the workplace or school</li>
                <li class="list-group-item">Promoting healing in children and teens after a traumatic death</li>
                <li class="list-group-item">Working with your grieving clients</li>
                <li class="list-group-item">Coping with death in the workplace or school</li>
              </ul>
    </div>

</div>
</div>


    </section>

    <!--================ Blog Area end =================-->

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

        <!-- link that opens popup -->

        <!-- JS here -->
        <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
    <script>
        $('.datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-calendar"></span>'
            }
        });

        $('.timepicker').timepicker({
            iconsLibrary: 'fontawesome',
            icons: {
                rightIcon: '<span class="fa fa-clock-o"></span>'
            }
        });
    </script>
</body>

</html>