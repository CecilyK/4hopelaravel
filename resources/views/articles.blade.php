<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.head')
</head>
<body>
   
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Articles</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->
    <section class="news-section">

      <div class="container">
       <!-- <div class="main-title-box text-center">
                     <div class="small-title">News & Blog</div>
                     <h2 class="big-title">Our Recent Artciles</h2>
                 </div> -->
                 <div class="row">
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0ms">
                         <div class="news-item">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/CKNmhMX/blog1.jpg.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Jan 03, 2019</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>The Future of Digital Marketing</h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Robert Tylor </a></span>
                                        
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="400ms">
                         <div class="news-item active">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/m5yGbdR/blog2.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Nov 05, 2018</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>Successful content marketing plan </h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Jessica keri</a></span>
                                        
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="800ms">
                         <div class="news-item">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/YXV3zmh/blog3.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Dec 22, 2018</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>Our agency helps to grow creativity</h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Brandon Auger</a></span>
                                        
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0ms">
                         <div class="news-item">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/CKNmhMX/blog1.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Dec 05, 2017</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>The Future of Digital Marketing</h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Astley Fletcher</a></span>
                                        
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="400ms">
                         <div class="news-item active">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/m5yGbdR/blog2.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Nov 13, 2017</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>Successful content marketing plan </h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Nichel Benjamin</a></span>
                                        
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                     <!--Single Blog Start-->
                     <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="800ms">
                         <div class="news-item">
                             <div class="news_box">
                                 <div class="newsimg"><img class="img-responsive" src="https://i.ibb.co/YXV3zmh/blog3.jpg" alt=""></div>
                                 <div class="news-content">
                                     <div class="news_postdate">
                                         <span>Aug 18, 2017</span>
                                     </div>
                                     <a href="{{ url('single-blog') }}">
                                         <h3>Our agency helps to grow creativity</h3>
                                     </a>
                                     <p>Lorem ipsum dolor sit amet, cons ectet a ur elit. Vestibulum necod ios suspe age a to ndisse cursus mal.</p>
                                     <div class="news_authorinfo">
                                         <span><i class="fa fa-user"></i>  <a href="">Andrew Mickel </a></span>
                                         <span><i class="fa fa-comment"></i> <a href="">Comments: (4)</a></span>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                     <!--Single Blog End-->
                 </div>
             </div>
         </div>
 </section>
  

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

  <!-- link that opens popup -->

 <!-- JS here -->
 <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
      




</body>
</html>
