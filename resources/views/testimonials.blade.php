<!DOCTYPE html>
<html lang="en">

<head>
@include('layouts.head')


</head>

<body>


    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Testimonials</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <div class="container gallery-container">


        <div class="tz-gallery">

            <div class="row">
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0ms">
                    <a class="lightbox" href="../images/park.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="400ms">
                    <a class="lightbox" href="../images/bridge.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-12 col-md-4 wow fadeInUp" data-wow-delay="800ms">
                    <a class="lightbox" href="../images/tunnel.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0ms">
                    <a class="lightbox" href="../images/coast.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="400ms">
                    <a class="lightbox" href="../images/rails.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="800ms">
                    <a class="lightbox" href="../images/traffic.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="0ms">
                    <a class="lightbox" href="../images/rocks.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="400ms">
                    <a class="lightbox" href="../images/benches.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4 wow fadeInUp" data-wow-delay="800ms">
                    <a class="lightbox" href="../images/sky.jpg">
                        <img src="https://unsplash.it/500" alt="">
                    </a>
                </div>
            </div>

        </div>

    </div>


    <script>
        baguetteBox.run('.tz-gallery');
    </script>

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

        <!-- link that opens popup -->

        <!-- JS here -->
        <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
</body>

</html>