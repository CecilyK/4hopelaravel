<!doctype html>
<html lang="en">

    <head>
    @include('layouts.head')
    <link rel="stylesheet" href="{{ secure_asset('css/intlTelInput.css') }}">
     
        <!-- <link rel="stylesheet" href="css/responsive.css"> -->
    </head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->
    <!-- bradcam_area_start  -->
    <div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="bradcam_text text-center">
                        <h3>Volunteer with Us</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- bradcam_area_end  -->

    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <!--<div class="d-none d-sm-block mb-5 pb-4">
                    <div id="map" style="height: 480px; position: relative; overflow: hidden;"></div>
                    <script>
                        function initMap() {
                            var uluru = {
                                lat: -25.363,
                                lng: 131.044
                            };
                            var grayStyles = [{
                                    featureType: "all",
                                    stylers: [{
                                            saturation: -90
                                        },
                                        {
                                            lightness: 50
                                        }
                                    ]
                                },
                                {
                                    elementType: 'labels.text.fill',
                                    stylers: [{
                                        color: '#ccdee9'
                                    }]
                                }
                            ];
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: {
                                    lat: -31.197,
                                    lng: 150.744
                                },
                                zoom: 9,
                                styles: grayStyles,
                                scrollwheel: false
                            });
                        }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&amp;callback=initMap">
                    </script>
    
                </div>-->
    
    
                <div class="row">
                    <div class="col-12">
                        <h2 class="contact-title">Apply for Volunteer</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your full name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'" placeholder="Enter your email address">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="phone" id="phone" type="tel" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter phone Number'" placeholder="Enter phone Number">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3>Nairobi, Kenya.</h3>
                                <p>Kimathi street, building 6</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3>070000000</h3>
                                <p>Mon to Fri 9am to 6pm</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3>youremail@gmail.com</h3>
                                <p>Reach us through email anytime</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- ================ contact section end ================= -->
    

    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.foooter')
    </footer>
    <!-- footer_end  -->

        <!-- link that opens popup -->

        <!-- JS here -->
        <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>
        <script>
            $('#datepicker').datepicker({
                iconsLibrary: 'fontawesome',
                icons: {
                 rightIcon: '<span class="fa fa-caret-down"></span>'
             }
            });
            $('#datepicker2').datepicker({
                iconsLibrary: 'fontawesome',
                icons: {
                 rightIcon: '<span class="fa fa-caret-down"></span>'
             }
    
            });
        </script>
       <script src="js/intlTelInput.js"></script>
       <script>
var input = document.querySelector("#phone");
window.intlTelInput(input);
</script>
    </body>
    
    </html>