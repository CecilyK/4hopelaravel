<!DOCTYPE html>
<html lang="en">

<head>
@include('layouts.head')
 
    <title>Events Section</title>
</head>

<body >

    
    <!-- header-start -->
    <header>
    @include('layouts.header')
    </header>
    <!-- header-end -->

<!-- bradcam_area_start  -->
<div class="bradcam_area breadcam_bg overlay d-flex align-items-center justify-content-center">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h3>Events and announcements</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- bradcam_area_end  -->

    <section style="background:url('img/banner/backg1.jpg');
    background-size: cover;">

        <div class="leftBox">
            <div class="content">
                <h1>Events and shows</h1>
                <p>Lorem ipsum dolor sit amet,distinctio dolore hic? Eius dolore animi ratione
                    facilis voluptas officiis molestias voluptatem?</p>
            </div>
        </div>

        <div class="events">
            <ul>
                <li>
                    <div class="time">
                        <h2>24<br><span>June</span></h2>
                    </div>
                    <div class="details">
                        <h3>Where does it come from</h3>
                        <p>Lorem ipsum dolor sit amet,distinctio dolore hic? Eius dolore animi ratione
                            </p>
                        <a href="#">View Details</a>
                    </div>
                    <div style="clear: both;"></div>
                </li>
                <li>
                    <div class="time">
                        <h2>24<br><span>June</span></h2>
                    </div>
                    <div class="details">
                        <h3>Where does it come from</h3>
                        <p>Lorem ipsum dolor sit amet,distinctio dolore hic? Eius dolore animi ratione
                            </p>
                        <a href="#">View Details</a>
                    </div>
                    <div style="clear: both;"></div>
                </li>
                <li>
                    <div class="time">
                        <h2>24<br><span>June</span></h2>
                    </div>
                    <div class="details">
                        <h3>Where does it come from</h3>
                        <p>Lorem ipsum dolor sit amet,distinctio dolore hic? Eius dolore animi ratione
                           </p>
                        <a href="#">View Details</a>
                    </div>
                    <div style="clear: both;"></div>
                </li>
            </ul>
        </div>
    </section>


    <!-- footer_start  -->
    <footer class="footer">
    @include('layouts.footer')
    </footer>
    <!-- footer_end  -->

        <!-- link that opens popup -->

        <!-- JS here -->
        <script src="{{ secure_asset('js/vendor/modernizr-3.5.0.min.js') }}"></script>
        <script src="{{ secure_asset('js/vendor/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ secure_asset('js/popper.min.js') }}"></script>
        <script src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ secure_asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ secure_asset('js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/ajax-form.js') }}"></script>
        <script src="{{ secure_asset('js/waypoints.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.counterup.min.js') }}"></script>
        <script src="{{ secure_asset('js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ secure_asset('js/scrollIt.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ secure_asset('js/wow.min.js') }}"></script>
        <script src="{{ secure_asset('js/nice-select.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.slicknav.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ secure_asset('js/plugins.js') }}"></script>
        <script src="{{ secure_asset('js/gijgo.min.js') }}"></script>
        <!--contact js-->
        <script src="{{ secure_asset('js/contact.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.ajaxchimp.min.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.form.js') }}"></script>
        <script src="{{ secure_asset('js/jquery.validate.min.js') }}"></script>
        <script src="{{ secure_asset('js/mail-script.js') }}"></script>

        <script src="{{ secure_asset('js/main.js') }}"></script>

    
</body>

</html>